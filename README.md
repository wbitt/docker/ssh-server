# The smallest SSH server container
The title says it all.

## Why?
Because, I needed a way to allow developers to forward MySQL port from inside a kubernetes cluster to a dev (or even ops) computer, without needing to give full `kubectl` access to the kubernetes cluster. Using this image as a sidecar with the MySQL db container, and by using SSH port-forwarding from the client side, in concert with public part of SSH key-pairs, anyone can be given access to the *"MySQL service only"*.

This container image can also be used in ssh *client* mode as a *tunnel-maker*. Explanation and example is further below in this document. 

## How to setup SSH tunnel with mysql using this ssh-server sidecar?

```
[kamran@kworkhorse ~]$ ssh -L 3306:127.0.0.1:3306 -p 32222 sshuser@34.142.115.100
mysql-0:~$
```

Now open another terminal and use normal mysql command to connect to this forwarded port:

```
[kamran@kworkhorse ~]$ mysql -h 127.0.0.1 -u dbadmin -p
Enter password: 

MariaDB [(none)]>

MariaDB [(none)]> show databases;
```

## What if I already have a local mysql instance running on port 3306?
If - for some reason - you have a local mysql instance on your local computer, then simply edit the command shown above, and assign a different port number to the forwarded port on local computer.

Example:

```
[kamran@kworkhorse ~]$ ssh -L 33060:127.0.0.1:3306 -p 32222 sshuser@34.142.115.100
mysql-0:~$
```

Then, use the mysql command with extra `-P 33060` parameter:


```
[kamran@kworkhorse ~]$ mysql -h 127.0.0.1 -P 33060 -u dbadmin -p
Enter password: 

MariaDB [(none)]> 
``` 

## Can "anyone" login to my SSH container?

Or, **"Can "anyone" get access to my DB service through this SSH container?"**

**No.** The only people who will ever be able to **login**, or use **port-forwarding** for this sevice will be the ones having their keys saved inside the git repository, which is used in this container. We maintain a single `authorized_keys` file in that repository, and pull only the master branch, which is locked and protected. So only the admins will be able to allow who gets in. The repository itself does not need to be private, as it only contains public keys anyway. Still, if you don't understand how *"public key authentication"* works, or if you are just paranoid, you can make the repository private to your team.

Of-course the entire thing will only work when you provide a link to the authorized_keys file as an environment variable. If you don't, then there are absolutely no keys in the container, and no one will ever be able to use it. A pretty useless situation! :)

**There are no default/hidden public keys placed in this image**. You can verify by examining the authorized_keys files in `/root/.ssh/` and `/home/sshuser/.ssh/` . It should be as safe as the underlying OS permits, which is Alpine Linux.

**Note:** The user (`sshuser`) has a completely random password assigned to it because of a technical difficulty. Without the password the account of "sshuser" remains locked. When the user account is *locked*, the user will not be able to login even if we setup only key-based login using authorized_keys file. Though there is not need to worry, because:

1. the password is completely random, generated at the time when the image was built,
2. password-based login is disabled completely in the SSH configuration.

The password is a random string, which *looks* like this:
 `ROmmepSe0Bx43YtI_2-IX3kpJ7AThmiIcsZdKXDRQ6PbyiqtFPrj0-9qRpMEHOcH` 

(Good luck breaking it! :)

Excerpt from the included/active `sshd_config` file in this container image:

```
PasswordAuthentication no
PubkeyAuthentication yes
AuthorizedKeysFile  .ssh/authorized_keys
```

## How does it work?
We keep a list of user's ssh keys in a (separate) git-repository/web-URL and use the **raw link** of the authorized_keys file from the master branch of that repository, by passing it as an environment variable (`AUTH_KEYS_URL`) to the container. The container pulls this file from the URL, and stores it as `/home/sshuser/.ssh/authorized_keys`. The user (from home/work/dev computer) then connects to this container using `sshuser` user account.

By default, the user is not allowed interactive shell login. If you want interactive login, you will need to setup another environment variable (`ALLOW_INTERACTIVE_LOGIN`).

## Environment variables used in this image:
### `AUTH_KEYS_URL` 
Example: `AUTH_KEYS_URL=https://gitlab.com/kamranazeem/public-ssh-keys/-/raw/master/authorized_keys`

**Note:** It needs to be a **raw** web-URL, which shows contents directly when accessed. The file itself doesn't necessarily need to be in a git repository, but it needs to be accessible over the internet, through some web-URL using HTTP/HTTPS.

### `ALLOW_INTERACTIVE_LOGIN`
Example: `ALLOW_INTERACTIVE_LOGIN=true`

**Note:** Any value other than the words `true` or `false` is ignored, and defaults to `false`.

### `TZ` (Time Zone)
You would want that any incoming SSH connections are logged with correct timestamp according to the timezone. That is why `tzdata` is added to the container. Use `TZ` environment variable to set (and use) timezone for your ssh container instance.

Example: `TZ=Europe/London`

**Note:** List of time zones can be obtained by viewing the contents of `/usr/share/zoneinfo/` .

### `SSH_TUNNEL_SERVER_IP`
The IP of the target SSH server. This is used when this image is used as a client to setup SSH tunnel. Most people would not need to use it. See example further below.

### `SSH_TUNNEL_SERVER_PORT`
The SSH port of the target SSH server. It is normally port `22`, but can be a different one too. This is used when this image is used as a client to setup SSH tunnel. Most people would not need to use it. See example further below.

## Test run / examples:

### Run without using any git-repository/web-URL for `authorized_keys`:
Here is a test run. The idea is that I should not be able to login.

```
[kamran@kworkhorse ssh-server]$ docker run -d kamranazeem/ssh-server
```

Notice, I can't connect, nor login:

```
[kamran@kworkhorse ssh-server]$ ssh sshuser@172.17.0.2
Received disconnect from 172.17.0.2 port 22:2: Too many authentication failures
Disconnected from 172.17.0.2 port 22
[kamran@kworkhorse ssh-server]$ 
```


### Using a git-repository/web-URL for `authorized_keys`: 

Local/test run:

```
[kamran@kworkhorse ssh-server]$ docker run \
  -e AUTH_KEYS_URL=https://gitlab.com/kamranazeem/public-ssh-keys/-/raw/master/authorized_keys  \
  -p 1022:22 \
  -d kamranazeem/ssh-server 
```

Notice, I can connect, but am not allowed an interactive shell/login. The helpful text below is coming from `/etc/motd` file.

```
[kamran@kworkhorse ssh-server]$ ssh sshuser@172.17.0.2
Welcome to Alpine based ssh-server!

. . . 

"Alpine Linux v3.10"
Connection to 172.17.0.2 closed.
[kamran@kworkhorse ssh-server]$ 
```

### Allow interactive shell/login:

```
[kamran@kworkhorse ssh-server]$ docker run \
  -e AUTH_KEYS_URL=https://gitlab.com/kamranazeem/public-ssh-keys/-/raw/master/authorized_keys \
  -e ALLOW_INTERACTIVE_LOGIN=true  \
  -e TZ=Europe/London \
  -p 1022:22 \
  -d kamranazeem/ssh-server 
```

Notice, this time I am able to connect, and login interactively.
```
[kamran@kworkhorse ssh-server]$ ssh sshuser@172.17.0.2
Welcome to Alpine based ssh-server!

. . . 

"Alpine Linux v3.10"

902b2ff30c4f:~$ date
Mon May 18 13:16:33 BST 2020
902b2ff30c4f:~$
```

### Example - Sidecar with postgresql - in Kubernetes:

Here is a Kubernetes YAML manifest for a statefulset used to deploy Postgres. In this example, this ssh-server is being used as a sidecar.

(Some details from the YAML file are skipped for simplicity reasons).

```
apiVersion: apps/v1

kind: StatefulSet
metadata:
  name: postgres
  labels:
    app: postgres
    tier: database
. . . 

      containers:
      - name: ssh-server 

        image: kamranazeem/ssh-server:3.10

        ports:
        - name: ssh 
          containerPort: 22
          
        env:
        - name: TZ
          value: "Europe/London"
        - name: AUTH_KEYS_URL
          value: "https://gitlab.com/witline/ssh-authorized-keys/-/raw/master/authorized_keys"
        - name: ALLOW_INTERACTIVE_LOGIN
          value: "true"

      - name: postgres
        image: postgres:14
        ports:
        - containerPort: 5432
          name: postgres
. . .

```

Here is the service file. Notice that the postgres service is only available within the Kubernetes cluster as `ClusterIP`, whereas the SSH service (sidecar), is available on a nodePort `22345`.

```
apiVersion: v1
kind: Service
metadata:
  name: postgres-ssh
  labels:
    app: postgres-ssh
    tier: database
spec:
  type: NodePort
  ports:
    - port: 22  
      targetPort: 22  
      nodePort: 22345  
  selector:
    app: postgres
    tier: database
---
apiVersion: v1
kind: Service
metadata:
  name: postgres
  labels:
    app: postgres
    tier: database
spec:
  type: ClusterIP
  ports:
    - port: 5432
      targetPort: 5432
  selector:
    app: postgres
    tier: database

```

On my local computer, I can now connect to the postgres service without needing Kubernetes access. 

First, on one terminal I bring a remote postgres port to my local computer:

```
[kamran@kworkhorse ~]$ ssh -N -L 5432:localhost:5432  -p 22345 sshuser@4.3.2.1
```

Then, on another terminal, I use standard `psql` command to connect to the remote database:

```
[kamran@kworkhorse ~]$ psql -h localhost -U dbadmin 
psql (15.4, server 14.1 (Debian 14.1-1.pgdg110+1))
Type "help" for help.

dbadmin=# \q
[kamran@kworkhorse ~]$ 
```


## Running this container as port-forwarder / tunnel mode - as ssh client:
This is a special case, for special needs, and is not for everyone. In this case, you would need to setup additional variables:

* `SSH_TUNNEL_SERVER_IP` - The IP of the target SSH server
* `SSH_TUNNEL_SERVER_PORT` - The SSH port of the target SSH server. It is normally port `22`, but can be a different one too.

Also, the default OS user in this container is `root`, and it will be this user in this container that will be making the SSH connection to the remote SSH server to create tunnel. So mount your (ed25519) SSH private key at `/root/.ssh/id_ed25519` , and **not** at `/home/sshuser/.ssh/id_ed25519` .

```
docker run  \
  -v /home/kamran/.ssh/id_ed25519:/root/.ssh/id_ed25519 \
  -e SSH_TUNNEL_SERVER_IP=1.2.3.4 \
  -e SSH_TUNNEL_SERVER_PORT=22345 \
  -d kamranazeem/ssh-server \
  ssh -N -L 5432:localhost:5432 -p 22345 sshuser@1.2.3.4 
```
**Note:** The public part of the private ssh-key used/mounted above should be in  `~/.ssh/authorized_keys` of whatever is the user name on the target SSH server defined as `SSH_TUNNEL_SERVER_IP`.

Instead of mounting the key as a volume, you can provide your SSH private key as a environment variable named `SSH_PRIVATE_KEY` . It is expected that your private key is of type `ed25519`. You will need to encode your key using `base64 -w 0` as shown below, and assign the encoded value to the `SSH_PRIVATE_KEY` variable .

```
[kamran@kworkhorse ~]$ SSH_PRIVATE_KEY=$(cat /home/kamran/.ssh/id_ed25519 | base64 -w 0)
```

```
docker run  \
  -e SSH_PRIVATE_KEY=$(cat /home/kamran/.ssh/id_ed25519 | base64 -w 0) \
  -e SSH_TUNNEL_SERVER_IP=1.2.3.4 \
  -e SSH_TUNNEL_SERVER_PORT=22345 \
  -d kamranazeem/ssh-server \
  ssh -N -L 5432:localhost:5432 -p 22345 sshuser@1.2.3.4 
```


------

### Acknowledgment/Credits: 
This SSH container is built on the ideas generated during a workshop with my friend [Salman Mukhtar](https://gitlab.com/mesalman).

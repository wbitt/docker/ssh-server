#!/bin/sh
# entrypoint for ssh-server

echo
echo "Updating /etc/motd with the version of Alpine Linux being used ..."
grep PRETTY_NAME /etc/os-release | sed 's/PRETTY_NAME=//' | tee -a /etc/motd
echo

# It expects an environment variable containing a web-link to a publicly available ssh-keys file.
# e.g. https://gitlab.com/kamranazeem/public-ssh-keys/-/raw/master/authorized_keys
# That file is then pulled and saved into ${SSH_USER_HOME_DIRECTORY}/.ssh/authorized_keys.
# There are other ways to provide this file too. 
#   For example by mounting a directory containing authorized_keys onto "${SSH_USER_HOME_DIRECTORY}/.ssh/" .
# When the main authorized_keys file is updated on the web-link (git repo),
#   the container can simply by re-started, which will pull latest keys.

SSH_USER_HOME_DIRECTORY="/home/sshuser"

AUTH_KEYS_PATH=${SSH_USER_HOME_DIRECTORY}/.ssh/authorized_keys

# The following two are used when this container is run in client mode,
#   to connect to some SSH server on the internet,
#   for port forwarding, therefore using that SSH server as a tunnel.
# The information is then used by ssh-keyscan to get fingerprint of this
#   tunnel server, which is then used by SSH client command - executed unattended.
# Without the presence of fingerprint, 
#   the ssh port forwarding command fails.

SSH_TUNNEL_SERVER_PORT=${SSH_TUNNEL_SERVER_PORT:-22}


if [ ! -z "${AUTH_KEYS_URL}" ]; then

  echo "Downloading public authentication 'authorized_keys' file from AUTH_KEYS_URL: ${AUTH_KEYS_URL} ..."

  wget -q -O ${AUTH_KEYS_PATH} ${AUTH_KEYS_URL}

  # The following WGET_EXIT_CODE check is added
  #   to ensure that the container starts
  #   only when the keys file is successfully downloaded.
  #   This means (and ensures) DNS to be working properly too.
  
  WGET_EXIT_CODE=$?
  
  while [ ${WGET_EXIT_CODE} -gt 0 ]; do 
    echo "WGET failed. Retrying in 1 second..."
    sleep 1
    wget -q -O ${AUTH_KEYS_PATH} ${AUTH_KEYS_URL}
    WGET_EXIT_CODE=$?
  done

  echo
  echo "Found $(egrep -v "^$|\#" ${AUTH_KEYS_PATH}| wc -l) keys in ${AUTH_KEYS_PATH} file."
  echo
  # There is no harm in displaying the public keys,
  #   so we show it.
  echo "----------------- START - ${AUTH_KEYS_PATH} --------------------"
  echo
  egrep -v "^$|\#" ${AUTH_KEYS_PATH}
  echo
  echo "------------------ END  - ${AUTH_KEYS_PATH} --------------------"
  chown sshuser:sshuser ${AUTH_KEYS_PATH}
else
  echo "The variable AUTH_KEYS_URL was found empty, and no SSH public keys can be obtained - just so you know!"
  echo "It means this SSH service/instance will not be accessible at all, and will be pretty useless!"
fi 

echo

if [ "${ALLOW_INTERACTIVE_LOGIN}" == "true" ]; then
  echo "ALLOW_INTERACTIVE_LOGIN was set to true. Enabling interactive login ..."
  usermod -s /bin/sh sshuser
else
  echo "ALLOW_INTERACTIVE_LOGIN was either empty, or set to value other than the word 'true'. Therefore this is set to false. Interactive login will not be available."
fi

echo

if [ ! -z "${SSH_TUNNEL_SERVER_IP}" ]; then
  echo "Obtaining SSH fingerprints of server ${SSH_TUNNEL_SERVER_IP} , on port ${SSH_TUNNEL_SERVER_PORT} ..."

  ssh-keyscan \
    -t dsa,ecdsa,ed25519,rsa \
    -p ${SSH_TUNNEL_SERVER_PORT} \
    ${SSH_TUNNEL_SERVER_IP} > /root/.ssh/known_hosts

  echo "Displaying obtained fingerprints of ${SSH_TUNNEL_SERVER_IP}:${SSH_TUNNEL_SERVER_PORT} ..."
  cat /root/.ssh/known_hosts

  # If SSH_PRIVATE_KEY env variable is set,
  #   then decode the key using base64 and create the private file at
  #   ${SSH_USER_HOME_DIRECTORY}/.ssh/id_ed25519 
  # The private key is expected to be of type ed25519 .
  #
  # Note: The SSH container allows incoming connections for user "sshuser" only.
  #       However, when this is used as a client, 
  #       then the user issuing the SSH commands from inside this container will be "root",
  #       and for that reason, the SSH_PRIVATE_KEY (if provided) has to be
  #       mounted / created at the location "/root/.ssh/id_ed25519"

  if [ ! -z ${SSH_PRIVATE_KEY} ]; then
    echo ${SSH_PRIVATE_KEY} | base64 -d > /root/.ssh/id_ed25519
  else
    echo "The variable SSH_PRIVATE_KEY was found empty."
    echo "If you are trying to use this container image as a client to create a tunnel to some other server,"
    echo "then, it is expected that either you setup SSH_PRIVATE_KEY with a base64 encoded ed25519 private key"
    echo "or, mount your ed25519 private key at this path in this container: /root/.ssh/id_25519"
  fi
  
fi

echo
echo "--------------- START - /etc/ssh/sshd_config ----------"
echo
egrep -v "^$|\#" /etc/ssh/sshd_config
echo
echo "--------------- END - /etc/ssh/sshd_config ------------"
echo
# Execute the actual command mentioned in CMD of the Dockerfile, to start SSHD process.
echo "Executing: $@ ..."
echo
exec "$@"
